use tokio::io::AsyncReadExt;
use tokio::net::TcpListener;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let listener = TcpListener::bind("0.0.0.0:8080").await?;
    println!("Server running on 127.0.0.1:8080...");

    loop {
        let (mut socket, _) = listener.accept().await?;
        tokio::spawn(async move {
            let mut buf = [0; 1024];

            let n = match socket.read(&mut buf).await {
                Ok(n) if n == 0 => return,
                Ok(n) => n,
                Err(e) => {
                    eprintln!("Failed to read from socket; erro = {:?}", e);
                    return;
                }
            };

            let data = String::from_utf8_lossy(&buf[0..n])
                .trim_end_matches('\n')
                .trim_end_matches('\r')
                .to_string();

            if data == "exit" {
                println!("Terminating...");
                std::process::exit(0);
            }

            println!("Received: {}", data);
        });
    }
}
